package ru.vpavlova.tm.repository;

import ru.vpavlova.tm.api.repository.IProjectRepository;
import ru.vpavlova.tm.entity.Project;

public class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

}
